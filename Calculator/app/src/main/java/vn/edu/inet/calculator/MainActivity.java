package vn.edu.inet.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends Activity {
    private final int MODEL_INPUT_NUMBER_1 = 0;
    private final int MODEL_INPUT_NUMBER_2 = 3;
    private final int MODEL_INPUT_NEXT_1 = 5;
    private final int MODEL_INPUT_NEXT_2 = 6;
    private final int MODEL_INPUT_OPERATOR = 1;
    private final int MODEL_RESULT = 2;
    private final int MODEL_ERROR = 4;
    private final int OPERATOR_PLUS = 11;
    private final int OPERATOR_MINUS = 12;
    private final int OPERATOR_NA = 13;
    private final int OPERATOR_DIV = 14;
    private final int OPERATOR_NONE = 10;

    private int status = MODEL_INPUT_NUMBER_1;
    private int operator = OPERATOR_NONE;

    private TextView tvResult;
    private Button btnCE, btnDel, btnDo, btnPlus, btnMinus, btnNa, btnDiv, btnResult, btnDot;
    private Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;

    private double input1, input2, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponent();
        clear();
    }

    private void initComponent() {
        input1 = input2 = result = 0;
        tvResult = (TextView) findViewById(R.id.tvResult);
        btnCE = (Button) findViewById(R.id.btnCE);
        btnDel = (Button) findViewById(R.id.btnDel);
        btnDo = (Button) findViewById(R.id.btnDo);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnNa = (Button) findViewById(R.id.btnNa);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btnResult = (Button) findViewById(R.id.btnResult);
        btnDot = (Button) findViewById(R.id.btnDot);
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        ArrayList<Button> listNumber = new ArrayList<Button>();
        listNumber.add(btn0);
        listNumber.add(btn1);
        listNumber.add(btn2);
        listNumber.add(btn3);
        listNumber.add(btn4);
        listNumber.add(btn5);
        listNumber.add(btn6);
        listNumber.add(btn7);
        listNumber.add(btn8);
        listNumber.add(btn9);
        ArrayList<Button> listOperator = new ArrayList<Button>();
        listOperator.add(btnPlus);
        listOperator.add(btnMinus);
        listOperator.add(btnNa);
        listOperator.add(btnDiv);

        btnCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        for (Button button : listNumber) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button b = (Button) v;
                    inputNumber(b.getText().toString());
                }
            });
        }
        for (Button button : listOperator) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button b = (Button) v;
                    inputOperator(textToOperator(b.getText().toString()));
                }
            });
        }
        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputDot();
            }
        });
        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputEqual();
            }
        });
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputDel();
            }
        });
        btnDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputDo();
            }
        });
    }

    private void clear() {
        tvResult.setText("0");
        input1 = input2 = result = 0;
        status = MODEL_INPUT_NUMBER_1;
        operator = OPERATOR_NONE;
    }

    private void inputNumber(String num) {
        if (status == MODEL_ERROR) {
            status = MODEL_INPUT_NUMBER_1;
            tvResult.setText("0");
        }

        if (status == MODEL_INPUT_NEXT_1) {
            status = MODEL_INPUT_NUMBER_2;
            tvResult.setText("0");
        } else if (status == MODEL_INPUT_NEXT_2) {
            clear();
            status = MODEL_INPUT_NUMBER_1;
            tvResult.setText("0");
        }

        if (status == MODEL_INPUT_NUMBER_1 || status == MODEL_INPUT_NUMBER_2) {
            String result = tvResult.getText().toString();
            if (result.length() < 11) {
                if (!result.equals("0")) {
                    tvResult.setText(result + num);
                } else {
                    tvResult.setText(num);
                }
            }
        }
    }

    private void inputDot() {
        String result = tvResult.getText().toString();
        if (result.indexOf(".") >= 0) {
            return;
        }
        switch (status) {
            case MODEL_INPUT_NUMBER_1:
                tvResult.setText(result + ".");
                break;
            case MODEL_INPUT_NUMBER_2:
                tvResult.setText(result + ".");
                break;
            case MODEL_INPUT_NEXT_1:
                tvResult.setText("0.");
                break;
            case MODEL_INPUT_NEXT_2:
                tvResult.setText("0.");
                break;
        }
    }

    private void inputOperator(int operator) {
        if (status == MODEL_INPUT_NUMBER_1) {
            this.operator = operator;
            input1 = getScreenValue();
            status = MODEL_INPUT_NEXT_1;
        } else if (status == MODEL_INPUT_NUMBER_2) {
            input2 = getScreenValue();
            result = operator();
            if (status != MODEL_ERROR) {
                tvResult.setText(formatResult(result + ""));
                status = MODEL_INPUT_NEXT_2;
                this.operator = operator;
                this.input1 = result;
            } else if (status == MODEL_ERROR) {
                tvResult.setText("ERROR");
            } else {
                this.operator = operator;
                input1 = getScreenValue();
                status = MODEL_INPUT_NEXT_1;
            }
        } else if (status == MODEL_INPUT_NEXT_2) {
//            this.operator = operator;
        }
    }

    private void inputEqual() {
        if (status == MODEL_INPUT_NUMBER_2) {
            input2 = getScreenValue();
            result = operator();
            if (status != MODEL_ERROR) {
                tvResult.setText(formatResult(result + ""));
                status = MODEL_INPUT_NEXT_2;
                this.input1 = result;
                operator = OPERATOR_NONE;
            } else {
                tvResult.setText("ERROR");
            }
        }
    }

    private void inputDel() {
        if (status == MODEL_INPUT_NUMBER_1 || status == MODEL_INPUT_NUMBER_2) {
            String result = tvResult.getText().toString();
            if (result.equals("0") || result.equals("0.")) {
                result = "0";
            } else {
                if (result.length() > 1) {
                    result = result.substring(0, result.length() - 1);
                } else {
                    result = "0";
                }
            }
            tvResult.setText(result);
        }
    }

    private void inputDo(){
        if (status == MODEL_INPUT_NUMBER_1 || status == MODEL_INPUT_NUMBER_2) {
            String result = tvResult.getText().toString();
            double re = Double.parseDouble(result);
            re = re*(-1);

            tvResult.setText(formatResult(re+""));
        }
    }
    private double getScreenValue() {
        return Double.parseDouble(tvResult.getText().toString());
//        return stringToFloat(tvResult.getText().toString());
    }

    private String formatResult(String r) {
        int x = r.indexOf(".");
        if (x >= 0) {
            String ends = r.substring(x);
            if (ends.equals(".0")) {
                return r.substring(0, x);
            }
        }
        return r;
    }

    public double operator() {

        switch (operator) {
            case OPERATOR_PLUS:
                return input2 + input1;
            case OPERATOR_MINUS:
                return input1 - input2;
            case OPERATOR_NA:
                return input1 * input2;
            case OPERATOR_DIV:
                if (input2 == 0) {
                    status = MODEL_ERROR;
                } else {
                    return input1 / input2;
                }
            case OPERATOR_NONE:
                status = MODEL_INPUT_NUMBER_1;
                return 0;
            default:
                status = MODEL_ERROR;
                return 0;
        }
    }

    private float stringToFloat(String str) {
        int x = str.indexOf(".");
        if (x < 0) {
            return Float.parseFloat(str);
        } else {
            float start = Float.parseFloat(str.substring(0, x));
            float end = Float.parseFloat(str.substring(x + 1));
            String ends = str.substring(x + 1);
            for (int i = 0; i < ends.length(); i++) {
                end /= 10;
            }
            return start + end;
        }
    }

    private int textToOperator(String ope) {
        if (ope.equals("+")) {
            return OPERATOR_PLUS;
        } else if (ope.equals("-")) {
            return OPERATOR_MINUS;
        } else if (ope.equals("x")) {
            return OPERATOR_NA;
        } else if (ope.equals("/")) {
            return OPERATOR_DIV;
        } else {
            return OPERATOR_NONE;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

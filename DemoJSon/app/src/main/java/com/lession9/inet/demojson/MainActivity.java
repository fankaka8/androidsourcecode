package com.lession9.inet.demojson;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    TextView tv_Name;
    TextView tv_Mail;
    ListView lst_Phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_Name = (TextView) findViewById(R.id.tv_Name);
        tv_Mail = (TextView) findViewById(R.id.tv_Mail);
        lst_Phone = (ListView) findViewById(R.id.lst_Phone);

        GetJSONClass jsonClass = (GetJSONClass) new GetJSONClass().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class GetJSONClass extends AsyncTask<Void, ArrayList<String>, ArrayList<String>> {

        String url = "http://api.androidhive.info/volley/person_object.json";
        @Override
        protected ArrayList<String> doInBackground(Void... params) {

            ArrayList<String> arr = new ArrayList<String>();

            ServiceUntil server = new ServiceUntil();
            String jsonStr = server.makeServiceCall(url, ServiceUntil.GET);

            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                arr.add("Name: " + jsonObj.getString("name"));
                arr.add("Email: " + jsonObj.getString("email"));

                JSONObject jsonPhoneObj = jsonObj.getJSONObject("phone");
                arr.add(jsonPhoneObj.getString("home"));
                arr.add(jsonPhoneObj.getString("mobile"));

                publishProgress(arr);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return arr;
        }

        @Override
        protected void onProgressUpdate(ArrayList<String>... values) {
            super.onProgressUpdate(values);

            tv_Name.setText(values[0].get(0));
            tv_Mail.setText(values[0].get(1));
        }

        @Override
        protected void onPostExecute(ArrayList<String> arrayList) {
            super.onPostExecute(arrayList);

            ArrayList<String> arr = new ArrayList<String>();

            for(int i = 0; i < arrayList.size() -2; i++) {
                arr.add(arrayList.get(i + 2));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, arr);
            lst_Phone.setAdapter(adapter);
        }
    }
}

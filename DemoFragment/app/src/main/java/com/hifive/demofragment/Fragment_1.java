package com.hifive.demofragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class Fragment_1 extends Fragment implements View.OnClickListener {

    Button bt_green, bt_red, bt_blue;
    RelativeLayout layout_background;
    public static int BLUE = R.color.blue;
    public static int GREEN = R.color.green;
    public static int RED = R.color.red;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.layout_fragment_1, container, false);
        bt_blue = (Button) mView.findViewById(R.id.bt_blue);
        bt_red = (Button) mView.findViewById(R.id.bt_red);
        bt_green = (Button) mView.findViewById(R.id.bt_green);
        layout_background = (RelativeLayout) mView.findViewById(R.id.layout_background);
        bt_blue.setOnClickListener(this);
        bt_red.setOnClickListener(this);
        bt_green.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_blue:
                setBackgroundColor(BLUE);
                break;
            case R.id.bt_green:
                setBackgroundColor(GREEN);
                break;
            case R.id.bt_red:
                setBackgroundColor(RED);
                break;
        }
    }

    private void setBackgroundColor(int color) {
        layout_background.setBackgroundColor(getResources().getColor(color));
    }
}

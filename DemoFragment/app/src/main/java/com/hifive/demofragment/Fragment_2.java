package com.hifive.demofragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Bi Nguyen on 01/06/2015.
 */
public class Fragment_2 extends Fragment implements View.OnClickListener{
    private Button bt_up, bt_down;
    private TextView tv_count;
    private int count = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.layout_fragment_2,container,false);
        bt_up = (Button)mView.findViewById(R.id.bt_up);
        bt_down = (Button)mView.findViewById(R.id.bt_down);
        bt_up.setOnClickListener(this);
        bt_down.setOnClickListener(this);
        tv_count = (TextView)mView.findViewById(R.id.tv_count);
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_up:
                count++;
                setText(count);
                break;
            case R.id.bt_down:
                count--;
                setText(count);
                break;
        }
    }

    private void setText(int count){
        tv_count.setText("Fragment "+count);
    }
}

package com.hifive.demofragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.TabHost;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadTabs();
//        Fragment fragment_1 = new Fragment_1();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//
//        transaction.replace(R.id.fragment_1, fragment_1);
//        transaction.addToBackStack(null);
//        transaction.commit();
    }
    public void loadTabs()
    {
        final TabHost tab=(TabHost) findViewById
                (android.R.id.tabhost);
        tab.setup();
        TabHost.TabSpec spec;
        spec=tab.newTabSpec("t1");
        spec.setContent(R.id.fragment_1);
        spec.setIndicator("Fragment 1");
        tab.addTab(spec);
        spec=tab.newTabSpec("t2");
        spec.setContent(R.id.fragment_2);
        spec.setIndicator("Fragment 2");
        tab.addTab(spec);
        tab.setCurrentTab(0);

    }
}

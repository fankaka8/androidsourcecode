package vn.edu.inet.simplelayout;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null){
            String layout_type = bundle.getString("layout_type");
            if (layout_type.equals("linear")){
                setContentView(R.layout.layout_linear);
            } else if (layout_type.equals("frame")){
                setContentView(R.layout.layout_frame);
            } else if (layout_type.equals("grid")){
                setContentView(R.layout.layout_grid);
            } else if (layout_type.equals("relative")){
                setContentView(R.layout.layout_relative);
            }
        } else {
            setContentView(R.layout.activity_main);

            final TextView tvHello = (TextView) findViewById(R.id.tvHello);
            tvHello.setText("Set text for TextView");
            String msg = tvHello.getText().toString();
            final EditText etEmail = (EditText) findViewById(R.id.etEmail);
            etEmail.setText("inet@gmail.com");
            String email = etEmail.getText().toString();
            Button btnSend = (Button) findViewById(R.id.btnSend);
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // do some thing here
                    tvHello.setText(etEmail.getText());
                }
            });

            Button btnLinear = (Button) findViewById(R.id.btnLinear);
            Button btnFrame = (Button) findViewById(R.id.btnFrame);
            Button btnGrid = (Button) findViewById(R.id.btnGrid);
            Button btnRelative = (Button) findViewById(R.id.btnRelative);

            btnLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    intent.putExtra("layout_type", "linear");
//                    finish();
                    startActivity(intent);
                }
            });
            btnFrame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    intent.putExtra("layout_type", "frame");
//                    finish();
                    startActivity(intent);
                }
            });
            btnGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    intent.putExtra("layout_type", "grid");
//                    finish();
                    startActivity(intent);
                }
            });
            btnRelative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    intent.putExtra("layout_type", "relative");
//                    finish();
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HelloHCN.Unitity
{
    class HCN
    {
        // creat chieu dai and chieu rong
        public int _chieudai;
        public int _chieurong;

        // HCN contrustor
        public HCN(int cd, int cr)
        {
            _chieudai = cd;
            _chieurong = cr;
        }

        // get and set chieudai and chieu rong
        public void setChieiDai(int cd)
        {
            _chieudai = cd;
        }

        public int getChieuDai()
        {
            return _chieudai;
        }

        public void setChieuRong(int cr)
        {
            _chieurong = cr;
        }

        public int getChieuRong()
        {
            return _chieurong;
        }

        // tinh dien tich
        public int TinhDienTich()
        {
            return (_chieudai * _chieurong);
        }

        // tinh Chu Vi
        public int TinhChuVi()
        {
            return (_chieudai + _chieurong) * 2;
        }
    }
}
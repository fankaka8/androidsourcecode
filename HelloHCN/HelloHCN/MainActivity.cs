﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using HelloHCN.Unitity;

namespace HelloHCN
{
    [Activity(Label = "HelloHCN", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            // get ID from Main.axml
            EditText edt_chieudai = FindViewById<EditText>(Resource.Id.edt_chieudai);
            EditText edt_chieurong = FindViewById<EditText>(Resource.Id.edt_chieurong);
            Button bnt_tinhdientich = FindViewById<Button>(Resource.Id.bnt_tinhdientich);
            Button bnt_tinhchuvi = FindViewById<Button>(Resource.Id.bnt_tinhchuvi);
            TextView tv_ketqua = FindViewById<TextView>(Resource.Id.tv_ketqua);

            // click event to calculator Dien Tich And Chu Vi
            // Tinh Dien Tich
            bnt_tinhdientich.Click += delegate {
                // get chieu dai and chieu rong from edittext
                // get chieu dai
                int chieudai = Int32.Parse(edt_chieudai.Text.ToString());
                // get chieu rong
                int chieurong = Int32.Parse(edt_chieurong.Text.ToString());

                // tinh dien tich HCN
                HCN hcn = new HCN(chieudai, chieurong); // create HCN with dai and rong from edittext
                int dientich = hcn.TinhDienTich();
                // set ket qua to textview
                tv_ketqua.Text = String.Format("Dien Tich HCN: {0}", dientich);
            };

            // Tinh Chu Vi when click to button TinhChuVi
            bnt_tinhchuvi.Click += delegate {
                // get chieu dai and chieu rong from edittext
                // get chieu dai
                int chieudai = Int32.Parse(edt_chieudai.Text.ToString());
                // get chieu rong
                int chieurong = Int32.Parse(edt_chieurong.Text.ToString());

                // tinh chu vi HCN
                HCN hcn = new HCN(chieudai, chieurong); // create HCN with dai and rong from edittext
                int chuvi = hcn.TinhChuVi();
                // set ket qua to textview
                tv_ketqua.Text = String.Format("Chu Vi HCN: {0}", chuvi);
            };
        }
    }
}


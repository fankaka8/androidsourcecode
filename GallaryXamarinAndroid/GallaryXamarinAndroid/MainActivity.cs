﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace GallaryXamarinAndroid
{
    [Activity(Label = "GallaryXamarinAndroid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            ViewFlipper flipper = FindViewById<ViewFlipper>(Resource.Id.viewFlipper);
            Button bntPre = FindViewById<Button>(Resource.Id.btnBack);
            ToggleButton bntPlay = FindViewById<ToggleButton>(Resource.Id.btnPlay);
            Button bntNext = FindViewById<Button>(Resource.Id.btnNext);

            bntPre.Click += delegate {
                if(checkStoping(flipper))
                {
                    flipper.ShowPrevious();
                }
            };

            bntNext.Click += delegate {
                if (checkStoping(flipper))
                {
                    flipper.ShowNext();
                }
            };

            bntPlay.Click += delegate {
                if (bntPlay.Checked)
                {
                    flipper.StartFlipping();
                } else
                {
                    flipper.StopFlipping();
                }
            };
        }

        public bool checkStoping(ViewFlipper flipper)
        {
            if(flipper.IsFlipping)
            {
                flipper.StopFlipping();
            }

            return true;
        }
    }
}


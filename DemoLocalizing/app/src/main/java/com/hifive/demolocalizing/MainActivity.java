package com.hifive.demolocalizing;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Locale;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    Button bt_vi, bt_en, bt_de, bt_ja;
    private Locale myLocale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_vi = (Button)findViewById(R.id.bt_vi);
        bt_en = (Button)findViewById(R.id.bt_en);
        bt_de = (Button)findViewById(R.id.bt_de);
        bt_ja = (Button)findViewById(R.id.bt_ja);
        bt_vi.setOnClickListener(this);
        bt_en.setOnClickListener(this);
        bt_de.setOnClickListener(this);
        bt_ja.setOnClickListener(this);
    }

    public void setMyLocale(String lang) {
        myLocale = new Locale(lang);
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration conf = resources.getConfiguration();
        conf.locale = myLocale;
        resources.updateConfiguration(conf, displayMetrics);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_vi:
                setMyLocale("vi");
                break;
            case R.id.bt_en:
                setMyLocale("en");
                break;
            case R.id.bt_de:
                setMyLocale("de");
                break;
            case R.id.bt_ja:
                setMyLocale("ja");
                break;
        }
    }
}

package com.lession1.inet.demooop;

/**
 * Created by My PC on 6/6/2015.
 */
public class Circle {
    protected int m_radius;

    public Circle(int radius) {
        m_radius = radius;
    }

    public int getRadius() {
        return m_radius;
    }

    public double getArea() {
        return Math.PI * m_radius * m_radius;
    }

    public double getCircum() {
        return Math.PI * 2 * m_radius;
    }
}

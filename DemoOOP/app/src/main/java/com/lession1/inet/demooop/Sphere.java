package com.lession1.inet.demooop;

/**
 * Created by My PC on 6/6/2015.
 */
public class Sphere extends Circle {

    public Sphere(int radius) {
        super(radius);
    }

    public double getArea() {
        return 4 * Math.PI * m_radius * m_radius;
    }

    public double getVolume() {
        return (4.0d / 3) * Math.PI * m_radius * m_radius * m_radius;
    }
}

package com.lession3.inet.customlistview;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    ArrayList<Student> arrStudent;
    CustomAdapter arrAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listView);

        arrStudent = new ArrayList<Student>();
        initArrStudent();

        arrAdapter = new CustomAdapter(getBaseContext(), arrStudent);
        listView.setAdapter(arrAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initArrStudent() {
        arrStudent.add(new Student("Tran Van A", "01/ 01/ 1989", R.drawable.av1));
        arrStudent.add(new Student("Tran Van B", "02/ 01/ 1989", R.drawable.av2));
        arrStudent.add(new Student("Tran Van C", "03/ 01/ 1989", R.drawable.av3));
        arrStudent.add(new Student("Tran Van D", "04/ 01/ 1989", R.drawable.av4));
    }
}

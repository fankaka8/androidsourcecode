package com.lession3.inet.customlistview;

/**
 * Created by My PC on 6/1/2015.
 */
public class Student {
    private String m_Name;
    private String m_Birthday;
    private int m_Avatar;

    public Student(String name, String birthday, int avatar) {
        this.m_Name = name;
        this.m_Birthday = birthday;
        this.m_Avatar = avatar;
    }

    public void setName(String name) {
        m_Name = name;
    }

    public void setBirthday(String birthday) {
        m_Birthday = birthday;
    }

    public void setAvatar(int avatar) {
        m_Avatar = avatar;
    }

    public String getName() {
        return m_Name;
    }

    public String getBirthday() {
        return m_Birthday;
    }

    public int getAvatar() {
        return m_Avatar;
    }
}

package com.lession3.inet.customlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by My PC on 6/1/2015.
 */
public class CustomAdapter extends BaseAdapter {

    //
    ArrayList<Student> arrStudent = new ArrayList<Student>();
    private static LayoutInflater inflater = null;

    public CustomAdapter(Context context, ArrayList<Student> arrStudent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrStudent = arrStudent;
    }

    @Override
    public int getCount() {
        return arrStudent.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewholder;

        View layout = convertView;

        if(layout == null) {
            viewholder = new ViewHolder();

            layout = inflater.inflate(R.layout.item_student, null);
            viewholder.tv_Name = (TextView) layout.findViewById(R.id.tv_Name);
            viewholder.tv_Birthday = (TextView) layout.findViewById(R.id.tv_Birthday);
            viewholder.img_Avatar = (ImageView) layout.findViewById(R.id.img_avatar);

            layout.setTag(viewholder);
        } else {
            viewholder = (ViewHolder)layout.getTag();
        }

        Student student = arrStudent.get(position);

        viewholder.tv_Name.setText(student.getName());
        viewholder.tv_Birthday.setText(student.getBirthday());
        viewholder.img_Avatar.setImageResource(student.getAvatar());

        return layout;
    }

    private class ViewHolder {
        private TextView tv_Name;
        private TextView tv_Birthday;
        private ImageView img_Avatar;
    }
}

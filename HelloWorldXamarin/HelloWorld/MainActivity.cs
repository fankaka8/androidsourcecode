﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace HelloWorld
{
	[Activity (Label = "HelloWorld", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
        // to check status of Button
		bool buttonClick = false;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            // Set main layout to display
			SetContentView (Resource.Layout.Main);

			// get ID component from file axml
            // get ID of Button from file Main.axml
			Button button = FindViewById<Button> (Resource.Id.bnt_starthello);
            // get ID of TextView from file Main.axml
            TextView textview = FindViewById<TextView> (Resource.Id.tv_helloworld);

            // Button event when click to Button
			button.Click += delegate {
				if(!buttonClick) {
                    // Change text for Button and TextView when Click
					button.Text = Resources.GetString(Resource.String.bnt_endthello);
                    textview.Text = Resources.GetString(Resource.String.tv_stoped);
				} else {
                    // Change text for Button and TextView when Click
                    button.Text = Resources.GetString(Resource.String.bnt_starthello);
                    textview.Text = Resources.GetString(Resource.String.tv_starting);
                }
                 
                // Change status of Button
                buttonClick = !buttonClick;
			};
		}
	}
}



package com.lession23.inet.singlelistview;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    ArrayList<String> dataOSArr;
    ArrayAdapter<String> dataArrAdapter;

    ListView lstview_OSdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataOSArr = new ArrayList<String>();
        String[] listData = getResources().getStringArray(R.array.listview_data);
        for(int i = 0; i < listData.length; i++) {
            dataOSArr.add(listData[i].toString());
        }

        dataArrAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                dataOSArr);

        lstview_OSdata = (ListView) findViewById(R.id.listViewOS);
        lstview_OSdata.setAdapter(dataArrAdapter);

        lstview_OSdata.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), "Index Of Item Click: " + position,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package vn.hifiveplus.trunglee.testservices;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Text;


public class MainActivity extends ActionBarActivity {

    TextView textService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textService = (TextView) findViewById(R.id.textService);

        GetDataFromServices services = (GetDataFromServices) new GetDataFromServices(textService).execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

class GetDataFromServices extends AsyncTask<Void, String, Void> {

    final String URL = "http://www.trunglee.somee.com/BirthdayCalendarServices.asmx?WSDL";

    TextView textService;
    public GetDataFromServices(TextView textService) {
        this.textService = textService;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        try
        {
            final String NAMESPACE="http://trunglee.com/";
            final String METHOD_NAME="getLstUser";
            final String SOAP_ACTION=NAMESPACE+METHOD_NAME;

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            SoapSerializationEnvelope envelope=
                    new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);

            //Nếu truyền số thực trên mạng bắt buộc phải đăng ký MarshalFloat
            //không có nó thì bị báo lỗi
            MarshalFloat marshal=new MarshalFloat();
            marshal.register(envelope);

            HttpTransportSE androidHttpTransport=
                    new HttpTransportSE(URL);
            androidHttpTransport.call(SOAP_ACTION, envelope);

            SoapPrimitive reposive = (SoapPrimitive) envelope.getResponse();

            publishProgress(reposive.toString());
/*
            //Get Array Catalog into soapArray
            SoapObject soapArray=(SoapObject) envelope.getResponse();

            //soapArray.getPropertyCount() return number of
            //element in soapArray
            //vòng lặp duyệt qua từng dòng dữ liệu
            for(int i=0; i<soapArray.getPropertyCount(); i++)
            {
                //(SoapObject) soapArray.getProperty(i) get item at position i
                SoapObject soapItem =(SoapObject) soapArray.getProperty(i);

                //soapItem.getProperty("CateId") get value of CateId property
                //phải mapp đúng tên cột:
                String id=soapItem.getProperty("_ID").toString();
                String name=soapItem.getProperty("_Name").toString();

                //đẩy vào array
                System.out.println("++++++++++++++++++++++++++++++ ID: " + id);
                System.out.println("++++++++++++++++++++++++++++++ Name: " + name);
            }
            //xác nhận cập nhật giao diện*/
        }
        catch(Exception e){
            System.out.println("++++++++++++++++++++++++++++++ Error: " + e.getMessage());
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        textService.setText(values[0].toString());
    }
}

package com.ex.sms;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.widget.Toast;

public class SMSSend {
	
	Context context;

	public SMSSend(Context context)
	{
		this.context = context;
	}

    private static final String ACTION_SENT = "com.ex.sms.SENT";
    private static final String ACTION_DELIVERED = "com.ex.sms.DELIVERED";
	
    public void sendSMS(String message, String phoneNumber)
	{
        PendingIntent sIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_SENT), 0);
        PendingIntent dIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_DELIVERED), 0);
        context.registerReceiver(sent, new IntentFilter(ACTION_SENT));
        context.registerReceiver(delivered,new IntentFilter(ACTION_DELIVERED)); 
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(phoneNumber, null, message,sIntent, dIntent);
    }
	 
    private BroadcastReceiver sent = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent) 
        {
            switch (getResultCode()) 
            {
	            case Activity.RESULT_OK:
	            	Toast.makeText(context,
	                        "send ok",
	                        Toast.LENGTH_SHORT).show();
	                break;
	            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
	            case SmsManager.RESULT_ERROR_NO_SERVICE:
	            case SmsManager.RESULT_ERROR_NULL_PDU:
	            case SmsManager.RESULT_ERROR_RADIO_OFF:
	            	Toast.makeText(context,
	                        "send error",
	                        Toast.LENGTH_SHORT).show();
	                break;
            }

            context.unregisterReceiver(this);
        }
    };

    private BroadcastReceiver delivered = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent) 
        {
            switch (getResultCode()) 
            {
	            case Activity.RESULT_OK:
	                break;
	            case Activity.RESULT_CANCELED:
	                break;
            }

            context.unregisterReceiver(this);
        }
    };
}

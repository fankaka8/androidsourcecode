package com.ex.sms;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class SMS_Reciver extends Activity {

	TextView tv_Message;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_sms_reciver);
		
		tv_Message = (TextView) findViewById(R.id.tv_msgReciver);
		
		tv_Message.setText(SMSReciver.getMsgReciver());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sms__reciver, menu);
		return true;
	}

}

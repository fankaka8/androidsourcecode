package com.ex.sms;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SMS_Send extends Activity {

	EditText edt_Phone;
	EditText edt_Msg;
	Button btn_Send;
	Button btn_Inbox;
	
	SMSSend smsSend;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_sms_sent);
		
		edt_Phone = (EditText) findViewById(R.id.id_Phone);
		edt_Msg = (EditText) findViewById(R.id.id_Msg);
		btn_Send = (Button) findViewById(R.id.btn_Send);
		btn_Inbox = (Button) findViewById(R.id.btn_Inbox);
		
		btn_Send.setOnClickListener(sendMsgListener);
		btn_Inbox.setOnClickListener(inboxListener);
		
		smsSend = new SMSSend(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sm, menu);
		return true;
	}

	OnClickListener sendMsgListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String msg = edt_Msg.getText().toString();
			String phoneNumber = edt_Phone.getText().toString();
			smsSend.sendSMS(msg, phoneNumber);
		}
	};

	OnClickListener inboxListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent intent = new Intent(getBaseContext(), SMS_Reciver.class);
			startActivity(intent);
		}
	};
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(getApplicationContext(), SMS_Reciver.class);
		startActivity(intent);
	};
}

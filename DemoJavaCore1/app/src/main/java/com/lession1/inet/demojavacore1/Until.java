package com.lession1.inet.demojavacore1;

/**
 * Created by My PC on 6/6/2015.
 */
public class Until {

    private static int m_number;

    public Until(int number) {
        m_number = number;
    }

    public boolean checkMod() {
        if(m_number % 2 != 0) {
            return true;
        } else {
            return false;
        }
    }

    public long getFactorial() {
        long fac = 1;

        for(int i = 2; i <= m_number; i++){
            fac *= i;
        }

        return fac;
    }

    public long getFactorialWhile() {
        long fac = 1;

        int index = 2;
        while (index <= m_number) {
            fac *= index;

            index++;
        }

        return fac;
    }

    public long getFactorialDoWhile() {
        long fac = 1;

        int index = 2;

        do {
            fac *= index;

            index++;
        } while (index < m_number);

        return fac;
    }

    public int getIndexNumber() {
        int index = 0;
        switch (m_number) {
            case 0:
                index = 0;

                break;
            case 1:
                index = 1;

                break;
            default:
                index = 2;

                break;
        }

        return index;
    }
}

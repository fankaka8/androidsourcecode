package com.lession7.inet.smsmanager;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.widget.Toast;

/**
 * Created by PhongVu on 2/1/2015.
 */
public class SMSManager {

    //
    Context m_context;
    String messageText = "";

    public static final String ACTION_SENT = "com.smsmanager.trung.smsmanager.SENT";
    public static final String ACTION_DELIVERED = "com.smsmanager.trung.smsmanager.DELIVERED";

    public  SMSManager(Context context) {
        m_context = context;
    }

    public void sendSMS(String linkText, String phoneNumber, String message) {
        messageText = linkText;

        PendingIntent sIntent = PendingIntent.getBroadcast(m_context, 0, new Intent(ACTION_SENT), 0);
        PendingIntent dIntent = PendingIntent.getBroadcast(m_context, 0, new Intent(ACTION_DELIVERED), 0);
        m_context.registerReceiver(smsSent, new IntentFilter(ACTION_SENT));
        m_context.registerReceiver(smsDelivered, new IntentFilter(ACTION_DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sIntent, dIntent);
    }

    public BroadcastReceiver smsSent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(m_context, "SMS sent", Toast.LENGTH_SHORT).show();

                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    SendLinkResult(false);

                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    SendLinkResult(false);

                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    SendLinkResult(false);

                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    SendLinkResult(false);

                    break;
            }

            m_context.unregisterReceiver(this);
        }
    };

    public BroadcastReceiver smsDelivered = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    // Toast.makeText(m_context, "SMS delivered", Toast.LENGTH_SHORT).show();
                    SendLinkResult(true);
                    break;
                case Activity.RESULT_CANCELED:
                    // Toast.makeText(m_context, "SMS not delivered", Toast.LENGTH_SHORT).show();
                    SendLinkResult(false);
                    break;
            }

            m_context.unregisterReceiver(this);
        }
    };

    public void SendLinkResult(boolean status) {
        String mss = "Ket qua";
        if(status) {
            mss = mss + " thanh cong";
        } else {
            mss = mss + " khong thanh cong";
        }

        Toast.makeText(m_context, mss, Toast.LENGTH_SHORT).show();
    }
}

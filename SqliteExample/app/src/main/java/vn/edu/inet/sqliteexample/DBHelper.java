package vn.edu.inet.sqliteexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by unmer_000 on 01/06/2015.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "INetSample.db";
    private final static int DB_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create table bookmark
        db.execSQL("CREATE TABLE IF NOT EXISTS bookmark (link TEXT, title TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            // do something

        }
    }

    public void insertBookMark(String link, String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("link", link);
        contentValues.put("title", title);
        db.insert("bookmark", null, contentValues);
        if (db.isOpen()) {
            db.close();
        }
    }

    public void deleteBookMark(String link, String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("bookmark", "link like '?' and title like '?'",
                new String[]{link, title});
        if (db.isOpen()) {
            db.close();
        }
    }

    public void updateBookMark(String linkOld, String linkNew, String titleNew) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("link", linkNew);
        contentValues.put("title", titleNew);
        db.update("bookmark", contentValues, "link like '?'",
                new String[]{linkOld});
        if (db.isOpen()) {
            db.close();
        }
    }

    public ArrayList<String> selectAllBookMark() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> result = new ArrayList<String>();
        Cursor cur = db.rawQuery("select * from bookmark", new String[]{});
        if (cur != null) {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                String link = cur.getString(0);
                String title = cur.getString(1);
                result.add(title + " - " + link);
                cur.moveToNext();
            }
        }
        if (db.isOpen()) {
            db.close();
        }
        return result;
    }
}

package com.lession8.inet.handlermultithreading;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    TextView tv_Result;
    ProgressBar br_bar;
    Button bnt_Start;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_Result = (TextView) findViewById(R.id.tv_Result);
        br_bar = (ProgressBar) findViewById(R.id.pr_bar);
        bnt_Start = (Button) findViewById(R.id.bnt_Start);

        handler = new Handler(){
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                br_bar.setProgress(msg.arg1);
                tv_Result.setText(msg.arg1 + " %");
            }
        };

        bnt_Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startProgressBar();
            }
        });
    }

    public void startProgressBar() {
        br_bar.setProgress(0);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 100; i++) {
                    Message msg = handler.obtainMessage();
                    msg.arg1 = i + 1;

                    SystemClock.sleep(200);
                    handler.sendMessage(msg);
                }
            }
        });

        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.hifiveplus.customlistviewdemo;

import android.content.Context;

/**
 * Created by My PC on 7/8/2015.
 */
public class Contact {
    int _avatar;
    String _name;
    String _age;

    public Contact(int _avatar, String _name, String _age) {
        this._avatar = _avatar;
        this._name = _name;
        this._age = _age;
    }

    public void setAvatar(int avatar) {
        _avatar = avatar;
    }

    public void setName(String name) {
        _name = name;
    }

    public void setAge(String age) {
        _age = age;
    }

    public int getAvatar() {
        return _avatar;
    }

    public String getName() {
        return _name;
    }

    public String getAge() {
        return  _age;
    }
}

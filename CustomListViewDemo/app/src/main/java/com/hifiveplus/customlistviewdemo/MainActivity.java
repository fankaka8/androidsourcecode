package com.hifiveplus.customlistviewdemo;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

    ListView lst_contact;
    Button bnt_search;
    EditText edt_search;

    ArrayList<Contact> arrayContact;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lst_contact = (ListView) findViewById(R.id.lst_cotact);
        bnt_search = (Button) findViewById(R.id.bnt_search);
        edt_search = (EditText) findViewById(R.id.edt_search);

        arrayContact = new ArrayList<Contact>();
        arrayContact.add(new Contact(R.drawable.av1, "Nguyen Van A", "20"));
        arrayContact.add(new Contact(R.drawable.av2, "Nguyen Van B", "21"));
        arrayContact.add(new Contact(R.drawable.av3, "Nguyen Van C", "22"));
        arrayContact.add(new Contact(R.drawable.av4, "Nguyen Van D", "23"));

        adapter = new CustomAdapter(this, arrayContact);

        lst_contact.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.hifiveplus.customlistviewdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by My PC on 7/8/2015.
 */
public class CustomAdapter extends BaseAdapter {

    Context _context;
    ArrayList<Contact> arrayContact;
    LayoutInflater inflater = null;

    public CustomAdapter(Context context, ArrayList<Contact> arr) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this._context = context;
        this.arrayContact = arr;
    }

    @Override
    public int getCount() {
        return arrayContact.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        View layout = convertView ;

        if(layout == null) {
            holder = new ViewHolder();
            layout = inflater.inflate(R.layout.layout_item, null);

            holder.avatar = (ImageView) layout.findViewById(R.id.img_avatar);
            holder.name = (TextView) layout.findViewById(R.id.tv_Name);
            holder.age = (TextView) layout.findViewById(R.id.tv_Age);

            layout.setTag(holder);
        } else {
            holder = (ViewHolder) layout.getTag();
        }

        Contact contact = arrayContact.get(position);
        holder.avatar.setImageResource(contact.getAvatar());
        holder.name.setText(contact.getName());
        holder.age.setText(contact.getAge());

        return layout;
    }

    class ViewHolder {
        ImageView avatar;
        TextView name;
        TextView age;
    }
}
